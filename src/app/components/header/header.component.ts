import {Component, OnInit, Input} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
    @Input() pageTitle: string;

    constructor(private router: Router) {
    }

    ngOnInit() {
        console.log('header works fine!');
    }


    openMyCart() {
        this.router.navigate(['my-cart']);
    }

}
