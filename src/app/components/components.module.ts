import {NgModule} from '@angular/core';
import {HeaderComponent} from './header/header.component';
import {IonicModule} from '@ionic/angular';

@NgModule({
    imports: [
        IonicModule.forRoot()
    ],
    declarations: [
        HeaderComponent
    ],
    exports: [
        HeaderComponent
    ]
})

export class ComponentsModule {
}
