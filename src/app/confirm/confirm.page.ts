import {Component, OnInit} from '@angular/core';
import {NavController} from '@ionic/angular';

@Component({
    selector: 'app-confirm',
    templateUrl: './confirm.page.html',
    styleUrls: ['./confirm.page.scss'],
})
export class ConfirmPage implements OnInit {
    title: string;

    constructor(private navCtrl: NavController) {
        this.title = 'confirm';

    }

    ngOnInit() {
    }

    cancel() {
        console.log('cancel');
        this.navCtrl.pop();
    }

}
