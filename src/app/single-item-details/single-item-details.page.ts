import {Component, OnInit, ViewChild} from '@angular/core';
import {IonSelect} from '@ionic/angular';

@Component({
    selector: 'app-single-item-details',
    templateUrl: './single-item-details.page.html',
    styleUrls: ['./single-item-details.page.scss'],
})
export class SingleItemDetailsPage implements OnInit {
    title: string;
    quantity: number;
    @ViewChild('typeSecection') typeSecection: IonSelect;
    @ViewChild('fatSelection') fatSelection: IonSelect;

    constructor() {
        this.title = 'indian goat';
        this.quantity = 1;
    }

    openSelect(id: string) {
        if (id === 'typeSecection') {
            this.typeSecection.open();
        } else {
            this.fatSelection.open();
        }
    }

    increment() {
        this.quantity++;
    }

    decrement() {
        while (this.quantity > 1) {
            this.quantity--;
        }
    }

    ngOnInit() {

    }

}
