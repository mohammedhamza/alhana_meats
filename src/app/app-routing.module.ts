import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  { path: 'single-item', loadChildren: './single-item/single-item.module#SingleItemPageModule' },
  { path: 'single-item-details', loadChildren: './single-item-details/single-item-details.module#SingleItemDetailsPageModule' },
  { path: 'my-cart', loadChildren: './my-cart/my-cart.module#MyCartPageModule' },
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: 'confirm', loadChildren: './confirm/confirm.module#ConfirmPageModule' },
  { path: 'country', loadChildren: './country/country.module#CountryPageModule' }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
