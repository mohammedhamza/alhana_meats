import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
    userName: string;
    password: any;

    constructor() {
    }

    ngOnInit() {
    }

    logIn() {
        console.log('logged in');
    }

}
