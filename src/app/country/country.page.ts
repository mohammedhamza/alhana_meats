import {Component, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Component({
    selector: 'app-country',
    templateUrl: './country.page.html',
    styleUrls: ['./country.page.scss'],
})
export class CountryPage implements OnInit {
    information: any;
    info: any;
    check: boolean;

    constructor(private http: HttpClient) {
        this.check = false;
    }

    ngOnInit() {
        this.http.get('../../assets/information.json').subscribe((value) => {
            this.info = value;
            this.information = this.info.items;
            console.log(this.information);
        });
    }


    toggleSection(i: number) {
        this.information.forEach((value) => {
            value.open = false;
        });
        this.information[i].open = !this.information[i].open;
    }

    checkCity(i: number, y: number) {
        this.information.forEach((value) => {
            value.children.forEach((child) => {
                child.check = false;
            });
        });
        this.information[i].children[y].check = !this.information[i].children[y].check;
    }

}
