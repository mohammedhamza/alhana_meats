import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import {IonicModule} from '@ionic/angular';

import {MyCartPage} from './my-cart.page';
import {ComponentsModule} from '../components/components.module';

const routes: Routes = [
    {
        path: '',
        component: MyCartPage
    }
];

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ComponentsModule,
        RouterModule.forChild(routes)
    ],
    declarations: [MyCartPage]
})
export class MyCartPageModule {
}
