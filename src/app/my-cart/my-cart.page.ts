import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-my-cart',
    templateUrl: './my-cart.page.html',
    styleUrls: ['./my-cart.page.scss'],
})
export class MyCartPage implements OnInit {
    title: string;

    constructor(private router: Router) {
        this.title = 'my cart';
    }

    ngOnInit() {
    }

    openConfirmPage() {
        this.router.navigate(['confirm']);
    }


}
