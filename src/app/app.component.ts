import {Component} from '@angular/core';

import {Platform, MenuController} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';
import {Router} from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    constructor(private platform: Platform,
                private splashScreen: SplashScreen,
                private statusBar: StatusBar,
                private router: Router,
                private menu: MenuController) {
        this.menu.enable(false, 'arabic');
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.statusBar.backgroundColorByHexString('#ffffff');
            this.splashScreen.hide();
        });
    }


    openPage(page) {
        this.router.navigate([page]).then(() => {
            this.menu.close();
        });
    }

    changeLang() {
        if (document.documentElement.dir === 'rtl') {
            document.documentElement.dir = 'ltr';
            this.menu.close().then(() => {
                this.menu.enable(false, 'arabic');
                this.menu.enable(true, 'english');
            });
        } else {
            document.documentElement.dir = 'rtl';
            this.menu.close().then(() => {
                this.menu.enable(true, 'arabic');
                this.menu.enable(false, 'english');
            });
        }
    }
}
