import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-single-item',
    templateUrl: './single-item.page.html',
    styleUrls: ['./single-item.page.scss'],
})
export class SingleItemPage implements OnInit {
    title: string;
    constructor(private router: Router) {
        this.title = 'indian goat';
    }

    ngOnInit() {
    }

    openItemDetails() {
        this.router.navigate(['single-item-details']);
    }

}
