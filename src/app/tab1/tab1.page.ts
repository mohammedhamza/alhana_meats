import {Component} from '@angular/core';
import {Platform, NavController} from '@ionic/angular';
import {AppMinimize} from '@ionic-native/app-minimize/ngx';
import {Router} from '@angular/router';

@Component({
    selector: 'app-tab1',
    templateUrl: 'tab1.page.html',
    styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    title: string;

    constructor(private platform: Platform, private appMinimize: AppMinimize, private router: Router, private navCtrl: NavController) {
        this.platform.backButton.subscribe(() => {
            this.navCtrl.pop();
        });

        this.title = 'goats';
    }

    navigate() {
        this.router.navigate(['/single-item']);
    }


}
